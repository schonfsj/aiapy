aiapy
======

.. automodapi:: aiapy
    :no-heading:
    :skip: test, UnsupportedPythonError
    :no-inheritance-diagram:
